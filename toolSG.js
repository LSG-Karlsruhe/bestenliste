const _ = {
    pipe(...functions) {
        return  args => functions.reduce((arg, fn) => fn(arg), args);
    },
    uniq(arr) {
        const set = new Set(arr.map(JSON.stringify));
        return Array.from(set).map(JSON.parse);
    },
    toTstamp(date) {
        if (null === date || isNaN(date))
        {
            return Math.round(new Date().getTime()/1000);
        } 
        else if(Object.prototype.toString.call(date) === '[object Date]') 
        {
            return Math.round(date.getTime()/1000);
        }
    },
    toDate(val, format) {
        if(format && typeof format === 'string'){
            let day, month, year;
            if(format.match(/d{2}/) !== null){
                day = val.split('.')[format.split('.').indexOf('dd')];
            }
            if(format.match(/m{2}/) !== null){
                month = val.split('.')[format.split('.').indexOf('mm')];
            }
            if(format.match(/y{4}/) !== null){
                year = val.split('.')[format.split('.').indexOf('yyyy')];
            }
            return new Date(year,month - 1,day);
        }
        return new Date(val * 1000);
    }
}

module.exports = _;
