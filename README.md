# lsg-bestenliste
Web scraper of running results for club members 

## Supported sites
* runtix
* RaceResult


## HowTo
1. Javascript  
2. npm run i  
3. URL der Ergebnisse in pageScrape.js schreiben, z.B. https://my.raceresult.com/175031/results?lang=de#0_0E3A3A  
4. npm run start  
5. SQL Selects werden erzeugt  

## Documentation

https://codeberg.org/LSG-Karlsruhe/bestenliste/wiki/_pages

