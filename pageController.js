
const _           = require('./toolSG');
const pageScraper = require('./pageScraper');
const pool        = require('./db');

const sqlDeleteOldAthletesBestFromDB    = (...q) => `DELETE FROM tl_lsg_best 
                                                        WHERE 
                                                            YEAR(FROM_UNIXTIME(date)) = YEAR(NOW()) AND 
                                                            athletes_id = ${q[0]}  AND 
                                                            distance = '${q[1]}'`;

const sqlAthletesBestFromDB             = (...q) => `SELECT * FROM tl_lsg_best 
                                                        WHERE YEAR(FROM_UNIXTIME(date)) = ${q[1].getFullYear()} AND 
                                                        athletes_id = ${q[0]} AND 
                                                        distance = '${q[2]}';`;

const sqlInsertAthletesBest             = (x, ...q) => `INSERT INTO 
                                                            tl_lsg_best (tstamp, distance, time, town, date, athletes_id, ak) 
                                                            VALUES (` + (_.toTstamp(new Date())) + `, 
                                                            '${q[0]}', 
                                                            '${x.web.time}', 
                                                            '${q[1]}', 
                                                            '${_.toTstamp(q[2])}', 
                                                            ${x.athlete ? x.athlete.id : 'NO_MATCHING_ID_FOUND_FOR_' + x.web.firstname + '_' + x.web.name}, 
                                                            '${x.athlete ? (x.athlete.gender==='m'?'m':'w') + findAK(x.athlete.born) : 'NO_MATCHING_AK_FOUND_FOR_' + x.web.firstname + '_' + x.web.name}');`;

const sqlAthletesFromDB                 = () => `SELECT * FROM tl_lsg_athlete WHERE active = 1;`;

async function querySQL(sql) {
    let conn;
    try {
        conn = await pool.getConnection();
        const result = await conn.query( sql );
        return result ? result : undefined;
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        conn.release();
    }
}

function findAthlete(web, lsgDB) {
    let athlete = lsgDB.find(lsg =>
        lsg.name.toLowerCase().trim() === web.name.toLowerCase().trim() &&
        lsg.firstname.toLowerCase().trim() === web.firstname.toLowerCase().trim()
    );

    // Assign not found athletes
    if(athlete === undefined){
        console.log("Error 'Couldn't find in LSG-DB': ", web);

        // Proposal table
        if(web.firstname.toLowerCase().trim().includes('miriam') && web.born === '1978'){
            athlete = lsgDB.find(lsg =>
                lsg.id === 395
                );
            }
        if(web.firstname.toLowerCase().trim().includes('theresia') && web.born === '1948'){
            athlete = lsgDB.find(lsg =>
                lsg.id === 121
                );
            }
        if(web.firstname.toLowerCase().trim().includes('harry') && web.born === '1943'){
            athlete = lsgDB.find(lsg =>
                lsg.id === 183
                );
            }
        if(web.firstname.toLowerCase().trim().includes('gudrun') || web.name.toLowerCase().trim().includes('gudrun') && web.born === '1955'){
            athlete = lsgDB.find(lsg =>
                lsg.id === 377
                );
            }
        console.log("Set to: ", athlete);
    }

    return athlete;
}

function findAK(born) {
    const age = new Date().getFullYear() - born;
    if (age < 30) {
        return 'hk';
    }
    return `${age - (age % 5)}`;
}

async function scrapeAll(browserInstance) {
    let browser;
    try {
        browser = await browserInstance;

        // Scrape table column names (runtix, RaceResult)
        const columnNames = await pageScraper.getTableColumnNames(browser);

        const indexFullName = columnNames.findIndex(
             (element) => element.includes('Name') || element.includes('Teilnehmer')
        );
        const indexNachname = columnNames.findIndex(
             (element) => element.startsWith('Nachname')
        );
        const indexVorname = columnNames.findIndex(
            (element) => element.startsWith('Vorname')
        );
        const indexVerein = columnNames.findIndex(
            (element) => element.includes('Verein')
        );
        const indexZeit = columnNames.findIndex(
            (element) => element === 'Zeit' || element === 'Brutto'
        );
        const indexBorn = columnNames.findIndex(
            (element) => element === 'Jg.' || element === 'Jg'
        );

        // Scrape date, location and distance
        const homepageTitle = await pageScraper.getDateAndLocation(browser);
        const dateLocation  = homepageTitle.split('|');
        const date          = _.toDate(dateLocation[0].split('-')[0].trim(' '),'dd.mm.yyyy');
        const location      = dateLocation[1].split(', ')[0].trim(' ');

        // Scrape distance
        const listGrouping = await pageScraper.getDistance(browser);
        const distance     = listGrouping.replace(' ', '').replace('21km', 'HM').replace('42km', 'Marathon');

        // Scrape athlets, only unique
        const scrapedRows = _.uniq(await pageScraper.scraper(browser));

        await browser.close();

        const lsgAthletes = await querySQL(sqlAthletesFromDB());

        const assignColumnNames = e => e.map(e => {
            if (pageScraper.url.includes('raceresult')) {
                const [id, , name, firstname, gender, born, club, time, place] = e;
                return {
                    id:        e[0],
                    name: e[indexNachname] ? e[indexNachname] :
                        (-1 !== e[indexFullName].indexOf(',')) ? e[indexFullName].split(',')[0].trim() :
                            e[indexFullName].split(' ')[1].trim(),
                    firstname: e[indexVorname] ? e[indexVorname] : 
                        (-1 !== e[indexFullName].indexOf(',')) ? e[indexFullName].split(',')[1].trim() :
                            e[indexFullName].split(' ')[0].trim(),
                    gender,
                    born:      e[indexBorn],
                    club:      e[indexVerein],
                    time:      e[indexZeit],
                    place
                }
            }
            if (pageScraper.url.includes('runtix')) {
                const [, , place, , fullname, club, agegroup, , , , time] = e;
                return {
                    name: e[indexFullName].split(',')[0].trim(),
                    firstname: e[indexFullName].split(',')[1].trim(),
                    agegroup: agegroup.toLowerCase(),
                    club,
                    time,
                    place
                }
            }
        });

        const findAthletesByClub = e => e.filter( e =>
            (e.club).toLowerCase().includes("lsg") && 
            (e.club).toLowerCase().includes("karlsruhe"));

        const checksumCount = (x) => { 
            console.log("Data Check: Number data sets found on 'web': ", x.length); 
            return x;
        };

        const compareAthletesWithDB = e => e.map( x => {
            return {
                web: x,
                athlete: findAthlete(x, lsgAthletes)
            }
        });

        const insertToDB = x => 
            querySQL(sqlInsertAthletesBest(x, distance, location, date)).then(
                    console.log(`Insert new PB: ${x.web.time} for ${x.web.firstname} ${x.web.name} ${x.athlete.id}`)
                    );

        const generateSQL = e => e.map(x => {
            if(x.athlete){
                querySQL(sqlAthletesBestFromDB(x.athlete.id, date, distance)).then(
                    athletesBest => {
                        if(athletesBest[0]){
                            athletesBest = athletesBest[0];
                            const placeholderDate = new Date().toDateString();

                            // Format Times to calculate Times with the Date() object.
                            const timeWeb = _.toTstamp(new Date(placeholderDate + " " + x.web.time.padStart(8, "00:").split(",")[0])); // padStart(): .padStart(8, "00:") // "42:27" -> "00:42:27" oder "1:16:15" -> "01:16:15"
                            const timeDB  = _.toTstamp(new Date(placeholderDate + " " + athletesBest.time.padStart(8, "00:").split(",")[0]));

                            if(timeWeb < timeDB){
                                console.log(`New PB on Web: ${x.web.time} (old PB in DB: ${athletesBest.time} in ${athletesBest.town}) for ${x.web.firstname} ${x.web.name} ${athletesBest.athletes_id}`);
                                
                                // temp
                                // querySQL(sqlDeleteOldAthletesBestFromDB(x.athlete.id, distance)).then( 
                                //     insertToDB(x)
                                // );
                            } else {
                                console.log(`Found faster old PB in DB: ${athletesBest.time} ${athletesBest.town} (Web time: ${x.web.time}) for ${x.web.firstname} ${x.web.name} ${athletesBest.athletes_id}`);
                            }
                        } else {
                            console.log(`First PB: ${x.web.time} for ${x.web.firstname} ${x.web.name} ${x.athlete.id}`);
                            
                            // temp insertToDB(x)
                        }
                    }
                )
                return `-- web: ${JSON.stringify(x.web)} -- db: ${JSON.stringify(x.athlete)}`;
            } 
            else 
            {
                console.log(`NO_MATCHING_ID_FOUND_FOR_${x.web.firstname}_${x.web.name}`);
            }
        });

        const result = _.pipe(
            assignColumnNames,
            findAthletesByClub,
            checksumCount,
            compareAthletesWithDB,
            generateSQL
            )(scrapedRows);

        // console.log(result);

    } catch (err) {
        console.log("Could not resolve the browser instance => ", err);
    }
}

module.exports = (browserInstance) => scrapeAll(browserInstance)
