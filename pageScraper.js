const scraperObject = {

  
    url: 'https://my.raceresult.com/301197/results#1_0727DB', // Karlsbad 2024
    // url: 'https://my.raceresult.com/282511/results#0_0E3A3A', // Oberwaldlauf 2024
    // url: 'https://my.raceresult.com/267966/#1_1140BF', // Turmberglauf 2024
    // url: 'https://my.raceresult.com/186270/', // FächerRUN
    // 'https://my.raceresult.com/167698/#0_1F1A9A', // viele TN und Zeit mit Zehntelsekunden
    // url: 'https://my.raceresult.com/163874/results#1_B341A8',
    // url: 'https://my.raceresult.com/163874/results#0_745764', // multi load more
    // url: 'https://runtix.com/sts/10050/1974/10/-/-',
    distance: '10km',

    async getDistance(browser) {
        let page = await browser.newPage();
        console.log(`Get distance from ${this.url} ... `);
        await page.goto(this.url);

        if(this.url.includes('raceresult')){
            // Wait for the required DOM to be rendered
            await page.waitForSelector('.MainTable');
            try {
                const list = await page.$eval(".MainTable .listGrouping", el => el.innerText);
                console.log(" ... ", list.toLowerCase().includes('km') ? list : this.distance);
                return list.toLowerCase().includes('km') ? list : this.distance;
            } catch (error) {
                console.log(error)
            } finally {
                return this.distance;
            }
        }
    },
    async getDateAndLocation(browser) {
        let page = await browser.newPage();
        console.log(`Get date and location from ${this.url} ...`);
        await page.goto(this.url);

        if(this.url.includes('raceresult')){
            // Wait for the required DOM to be rendered
            await page.waitForSelector('.MainTable');

            const date = [];
            const span = await page.$$(".HomepageHeadTitle span");
            for (const content of span) {
                const gameName = await page.evaluate(content => content.innerText, content);
                date.push(gameName);
            }
            console.log(" ... ", date[0]);
            return date[0];
        }
    },
    async getTableColumnNames(browser) {
        let page = await browser.newPage();
        console.log(`Get table column names from ${this.url} ...`);
        await page.goto(this.url);

        if(this.url.includes('raceresult')){
            // Wait for the required DOM to be rendered
            await page.waitForSelector('.MainTable');

            const th = [];
            const list = await page.$$(".MainTable th");
            for (const content of list) {
                const gameName = await page.evaluate(content => content.innerText, content);
                th.push(gameName);
            }
            console.log(" ... ", th);
            return th;
        }
    },
    async scraper(browser) {
        let page = await browser.newPage();
        console.log(`Scrape page ${this.url} ...`);
        await page.goto(this.url);

        if(this.url.includes('raceresult')){
            // Wait for the required DOM to be rendered
            await page.waitForSelector('.MainTable');

            const result = await page.evaluate(async() => {
                const elements = document.querySelector('.MainTable').querySelectorAll('.aShowAll');
    
                for (const element of elements) {
                    element.click();
                    await new Promise((resolve) => setTimeout(resolve, 2000));
                }
    
                // page doesn’t exist because of .evaluate() https://stackoverflow.com/questions/51448914/how-to-click-on-all-elements-and-wait-for-each-of-them-to-finish-their-ajax-requ
                const rows = document.querySelector('.MainTable').querySelectorAll('tr');
    
                const data = [];
                [].forEach.call(rows, function(rows) {
                    data.push(rows);
                });

                return data; // Return our data array
            });

            return result
                .filter(el => typeof el.arrPointer === 'object' && el.arrPointer !== undefined)
                .map(el => el.arrPointer);
        }
        if(this.url.includes('runtix')){
            // Wait for the required DOM to be rendered
            await page.waitForSelector('.results');

           return await page.evaluate(
                () => Array.from(
                    document.querySelectorAll('.results  tbody  tr'),
                    row => Array.from(row.querySelectorAll('th, td'), cell => cell.innerText)
                )
              );
        }
    }
}

module.exports = scraperObject;
